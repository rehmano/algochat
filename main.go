package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/x509"
	"database/sql"
	"fmt"
	"github.com/algorand/go-algorand-sdk/client/algod"
	"github.com/algorand/go-algorand-sdk/client/algod/models"
	"github.com/algorand/go-algorand-sdk/crypto"
	"github.com/algorand/go-algorand-sdk/mnemonic"
	"github.com/algorand/go-algorand-sdk/transaction"
	cp "github.com/atotto/clipboard"
	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
	_ "github.com/mattn/go-sqlite3"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

var algodAddress = os.Getenv("ALGOAPI_ADDRESS")
var psToken = os.Getenv("ALGOAPI_KEY")

const CA_ADDRESS = "3L4CTWNLKLY5HX257W3OKXO5AVWFV7D3SGUEA5RNHJ7N5FEBNRGTN47ORM"

/*
	Name: Syed "Omar" Rehman
	Undergraduate Computer Science Capstone
	Capstone Advisor: Dr. Michael J. Reale
	Fall 2020
*/

func main() {
	mainLoop()
}

/**
Starts up basic TUI + gets config dir setup if doesn't exist.
*/
func mainLoop() {
	var headers []*algod.Header
	headers = append(headers, &algod.Header{"X-API-Key", psToken})

	algodClient, err := algod.MakeClientWithHeaders(algodAddress, "", headers)
	checkError("Failed to make algod client", err)

	firstStart(algodClient)

	algochatDB, _ := sql.Open("sqlite3", os.Getenv("HOME")+"/.config/algochat/algochat.db")
	defer algochatDB.Close()

	mnemonic, address, pubKey, _ := getAccount(algochatDB)

	if err := ui.Init(); err != nil {
		log.Fatalf("failed to initialize termui: %v", err)
	}
	defer ui.Close()

	l := widgets.NewList()
	l.Title = "AlgoTUI"
	l.Rows = []string{
		"[0] Help",
		"[1] Copy address",
		"[2] Copy mnemonic",
		"[3] CA Registration",
		"[4] Chat",
	}
	l.TextStyle = ui.NewStyle(ui.ColorYellow)
	l.WrapText = false
	l.BorderStyle.Fg = ui.ColorGreen

	p := widgets.NewParagraph()
	p.Text = "Welcome to Algorand Chat\n" +
		"------------------------------\n" +
		"A decentralized secure messaging application based on the Algorand blockchain.\n"
	p.WrapText = true

	termWidth, termHeight := ui.TerminalDimensions()
	grid := ui.NewGrid()
	grid.SetRect(0, 0, termWidth, termHeight)
	grid.Set(
		ui.NewCol(1.0/3, l),
		ui.NewCol(1.0/1.5, p),
	)
	ui.Render(grid)

	previousKey := ""
	uiEvents := ui.PollEvents()
	for {
		e := <-uiEvents
		switch e.ID {
		case "<C-c>":
			return
		case "j", "<Down>":
			l.ScrollDown()
		case "k", "<Up>":
			l.ScrollUp()
		case "<C-d>":
			l.ScrollHalfPageDown()
		case "<C-u>":
			l.ScrollHalfPageUp()
		case "<C-f>":
			l.ScrollPageDown()
		case "<C-b>":
			l.ScrollPageUp()
		case "<Enter>":
			switch l.SelectedRow {
			case 0:
				helpString := "\nSteps to successfully send a message / receive messages:\n" +
					"[1] Print your address and mnemonic\n" +
					"[2] Load up the account with some Algos. ($1 worth should be enough)\n" +
					"[3] Register with the CA.\n" +
					"[4] Add address to your contact book. Chat -> <n>.\n" +
					"[5] Select the address and hit <Enter>.\n" +
					"[6] Type message and hit <Enter>, hit <Escape> to go back to list.\n" +
					"[7] <q> to leave chat menu. <Ctrl-c> to exit main menu.\n"
				if !strings.Contains(p.Text, helpString) {
					p.Text = p.Text + helpString
				}
			case 1:
				p.Text = p.Text +
					"\nAddress: " + address + "\n" +
					"Address has been copied to your clipboard.\n"
				_ = cp.WriteAll(address)
			case 2:
				p.Text = p.Text +
					"\nMnemonic: " + mnemonic + "\n" +
					"Mnemonic has been copied to your clipboard.\n"
				_ = cp.WriteAll(mnemonic)
			case 3:
				if checkIfRegistered(address, algodClient, pubKey) {
					p.Text = p.Text +
						"\nAddress already registered with the CA!\n" +
						"Pubkey confirmed the same!\n"
				} else {
					registerToCA(algochatDB, algodClient)
					p.Text = p.Text + "\nAddress Registered with the CA!\n"
				}
			case 4:
				handleChatMenu(algochatDB, algodClient)
			}
		case "g":
			if previousKey == "g" {
				l.ScrollTop()
			}
		case "<Home>":
			l.ScrollTop()
		case "G", "<End>":
			l.ScrollBottom()
		case "<Resize>":
			payload := e.Payload.(ui.Resize)
			grid.SetRect(0, 0, payload.Width, payload.Height)
			ui.Clear()
		}

		if previousKey == "g" {
			previousKey = ""
		} else {
			previousKey = e.ID
		}

		ui.Render(grid)
	}

}

/**
Handles the chat portion of the TUI.
*/
func handleChatMenu(db *sql.DB, client algod.Client) {
	l := widgets.NewList()
	l.Title = "<Escape>"
	l.Rows = []string{
		"Add a new address",
		"Retrieve messages",
	}
	accounts := getAccountsFromBook(db)
	for i, s := range accounts {
		l.Rows = append(l.Rows, "["+strconv.Itoa(i)+"]"+" "+s.Nickname)
	}
	l.TextStyle = ui.NewStyle(ui.ColorYellow)
	l.WrapText = false
	l.BorderStyle.Fg = ui.ColorGreen
	termWidth, termHeight := ui.TerminalDimensions()

	p := widgets.NewParagraph()
	p.Text = ""

	textBox := widgets.NewParagraph()
	textBox.Text = "Input: "
	textBox.Title = "<Ctrl-d> OR <Enter>"
	textBox.WrapText = true

	uiEvents := ui.PollEvents()
	previousKey := ""

	grid := ui.NewGrid()
	grid.SetRect(0, 0, termWidth, termHeight)
	grid.Set(
		ui.NewCol(1.0/4, l),
		ui.NewCol(0.75, ui.NewRow(0.9, p), ui.NewRow(0.1, textBox)),
	)
	ui.Render(grid)
	for {
		select {
		case e := <-uiEvents:
			switch e.ID {
			case "q":
				return
			case "j", "<Down>":
				l.ScrollDown()
			case "k", "<Up>":
				l.ScrollUp()
			case "<Enter>", "<C-d>":
				if l.SelectedRow == 0 {
					//New address
					getInputForNewAddress(p, textBox, l, grid, client, db)

				} else if l.SelectedRow == 1 {
					//Retrieve
					addNewMessages(db, client)
					p.Text = "Successfully retrieved new messages if any."
				} else {
					getInput(p, textBox, l, grid, db, client)
				}
			case "g":
				if previousKey == "g" {
					l.ScrollTop()
				}
			case "<Home>":
				l.ScrollTop()
			case "G", "<End>":
				l.ScrollBottom()
			case "<Resize>":
				payload := e.Payload.(ui.Resize)
				grid.SetRect(0, 0, payload.Width, payload.Height)
				ui.Clear()
			}

			if previousKey == "g" {
				previousKey = ""
			} else {
				previousKey = e.ID
			}
			l.BorderStyle.Fg = ui.ColorGreen
			p.BorderStyle.Fg = ui.ColorWhite
			textBox.BorderStyle.Fg = ui.ColorWhite
			ui.Render(grid)
		}
	}
}

/**
Different function for the 1st entry in the list, adding a new address.
*/
func getInputForNewAddress(p *widgets.Paragraph, textBox *widgets.Paragraph, l *widgets.List, grid *ui.Grid, client algod.Client, db *sql.DB) {
	l.BorderStyle.Fg = ui.ColorWhite
	p.BorderStyle.Fg = ui.ColorBlue
	textBox.BorderStyle.Fg = ui.ColorGreen
	addTitleToWholeTextBox(p, grid, l.Rows[l.SelectedRow])
	p.Text = ""
	ui.Render(grid)
	var i = 0
	var account AccountFromBook
	for {
		switch i {
		case 0:
			addTextToWholeTextBox(p, grid, "Please enter the address to add to your contacts.\n")
		case 1:
			addTextToWholeTextBox(p, grid, "They're registered with the CA. Now enter a nickname for this person.\n")
		case 2:
			addTextToWholeTextBox(p, grid, "Successfully added. Now you can find them in the contacts.\n")
		}
		wordToSend, isEnter := addTextToTextBox(textBox, grid, l.Rows[0]) //For the input box
		if isEnter && wordToSend != "" {
			addTextToWholeTextBox(p, grid, "Me: "+wordToSend) //Should be blocking for this
			if i == 0 {
				if !checkIfAddressRegistered(wordToSend, client) {
					addTextToWholeTextBox(p, grid, "\nCan't find registration with the CA for entered address. Check your spelling or try a different address.")
					continue
				} else {
					account.Address = wordToSend
					account.PublicKey = x509.MarshalPKCS1PublicKey(getPubKeyForAddress(wordToSend, client)) //Bytes it up
				}
			}
			if i == 1 {
				account.Nickname = wordToSend
			}
			i++
		}
		if !isEnter {
			break
		}
		if account.Nickname != "" {
			addAccountToBook(db, account)
			accounts := getAccountsFromBook(db)
			l.Rows = []string{
				"Add a new address",
				"Retrieve messages",
			}
			for i, s := range accounts {
				l.Rows = append(l.Rows, "["+strconv.Itoa(i)+"]"+" "+s.Nickname)
			}
			ui.Render(grid)
		}
	}

	clearWholeTextBox(p, grid)
}

/**
Sending messages to other addresses
*/
func getInput(p *widgets.Paragraph, textBox *widgets.Paragraph, l *widgets.List, grid *ui.Grid, db *sql.DB, client algod.Client) {
	l.BorderStyle.Fg = ui.ColorWhite
	p.BorderStyle.Fg = ui.ColorBlue
	textBox.BorderStyle.Fg = ui.ColorGreen
	accountToGetMessage := getAddressFromNicknameFromBook(l.Rows[l.SelectedRow][4:], db)
	chatMessages := getMessagesForAddressSortedFromTableASC(db, accountToGetMessage.Address)
	p.Text = ""
	for _, chatMessage := range chatMessages {
		if chatMessage.Direction == "From" {
			p.Text = p.Text + l.Rows[l.SelectedRow][4:] + ": " + chatMessage.Message + "\n"
		} else {
			p.Text = p.Text + "Me: " + chatMessage.Message + "\n"
		}

	}
	ui.Render(grid)
	mnemonic, address, _, _ := getAccount(db)
	addTitleToWholeTextBox(p, grid, l.Rows[l.SelectedRow][4:]) //Need to grab address for nickname
	for {
		wordToSend, isEnter := addTextToTextBox(textBox, grid, l.Rows[0])
		if isEnter && wordToSend != "" {
			addressToSendMessage := getAddressFromNicknameFromBook(l.Rows[l.SelectedRow][4:], db)
			go addTextToWholeTextBox(p, grid, "Me: "+wordToSend)
			go sendMessageToAddress(addressToSendMessage.Address, address, mnemonic, wordToSend, client, db)
		}
		if !isEnter {
			break
		}
	}
	clearWholeTextBox(p, grid)
}

var badButtons = []string{
	"<MouseLeft>", "<MouseMiddle>", "<MouseRight>", "<MouseRelease>", "<MouseWheelUp>", "<MouseWheelDown>", "<F1>",
	"<F2>", "<F3>", "<F4>", "<F5>", "<F6>", "<F7>", "<F8>", "<F9>", "<F10>", "<F11>", "<F12>", "<Insert>",
	"<Delete>", "<Home>", "<End>", "<PageUp>", "<PageDown>", "<Left>", "<Right>", "<C-<Space>>", "<C-<Backspace>>",
	"<Up>", //Should implement
	"<Down>",
}

func addTextToTextBox(textbox *widgets.Paragraph, grid *ui.Grid, address string) (string, bool) {
	for {
		uiEventsText := ui.PollEvents()
		select {
		case p := <-uiEventsText:
			wasBadChar := false
			if Contains(badButtons, p.ID) {
				continue
			}
			switch p.ID {
			case "<C-v>":
				pastedString, _ := cp.ReadAll()
				textbox.Text = textbox.Text + pastedString
				ui.Render(grid)
				wasBadChar = true
			case "<Tab>":
				textbox.Text = textbox.Text + "    "
				ui.Render(grid)
				wasBadChar = true
			case "<Enter>":
				wordToReturn := textbox.Text[len("Input: "):]
				textbox.Text = "Input: "
				ui.Render(grid)
				return wordToReturn, true
			case "<Escape>":
				return "", false
			case "<Space>":
				textbox.Text = textbox.Text + " "
				wasBadChar = true
				ui.Render(grid)
			case "<Backspace>":
				sizeOfText := len(textbox.Text)
				if sizeOfText > len("Input: ") {
					textbox.Text = textbox.Text[:sizeOfText-1]
				}
				wasBadChar = true
				ui.Render(grid)
			case "<Resize>":
				payload := p.Payload.(ui.Resize)
				grid.SetRect(0, 0, payload.Width, payload.Height)
				ui.Clear()
				ui.Render(grid)
				wasBadChar = true
			}
			if wasBadChar {
				break
			}
			textbox.Text = textbox.Text + p.ID
			ui.Render(grid)
		}
	}
}

func addTitleToWholeTextBox(textbox *widgets.Paragraph, grid *ui.Grid, titleToAdd string) {
	textbox.Title = titleToAdd
	ui.Render(grid)
}

func clearWholeTextBox(textbox *widgets.Paragraph, grid *ui.Grid) {
	textbox.Text = ""
	textbox.Title = ""
	ui.Render(grid)
}

func addTextToWholeTextBox(textbox *widgets.Paragraph, grid *ui.Grid, textToAdd string) {
	if textbox.Text != "" {
		textbox.Text = textbox.Text + textToAdd + "\n"
	} else {
		textbox.Text = textToAdd
	}
	ui.Render(grid)
}

func Contains(arrayToSearch []string, item string) bool {
	for _, i := range arrayToSearch {
		if i == item {
			return true
		}
	}
	return false
}

func registerToCA(db *sql.DB, client algod.Client) {
	mnemonic, address, publickey, _ := getAccount(db)

	//fmt.Println("###### public key: ", publickey)
	sendTransaction(CA_ADDRESS, address, mnemonic, publickey, client)
}

func checkIfRegistered(address string, client algod.Client, pubkey []byte) bool {
	txParams, err := client.SuggestedParams()

	checkError("Error getting suggested tx params", err)
	txs, _ := client.TransactionsByAddr(CA_ADDRESS, 0, txParams.LastRound+100)

	for _, tx := range txs.Transactions {
		if tx.From == address {
			//fmt.Println("Already registered with CA...")
			pubkeyGot, _ := x509.ParsePKCS1PublicKey(tx.Note)
			pubkeyGiven, _ := x509.ParsePKCS1PublicKey(pubkey)
			if pubkeyGot.Equal(pubkeyGiven) {
				return true
			}
		}
	}
	return false
}

func getPubKeyForAddress(address string, client algod.Client) *rsa.PublicKey {
	txParams, err := client.SuggestedParams()

	checkError("Error getting suggested tx params", err)
	txs, _ := client.TransactionsByAddr(CA_ADDRESS, 0, txParams.LastRound+100)

	for _, tx := range txs.Transactions {
		if tx.From == address {
			//fmt.Println("Already registered with CA...")
			pubkeyGot, _ := x509.ParsePKCS1PublicKey(tx.Note)
			return pubkeyGot
		}
	}
	return nil
}

func checkIfAddressRegistered(address string, client algod.Client) bool {
	txParams, err := client.SuggestedParams()

	checkError("Error getting suggested tx params", err)
	txs, _ := client.TransactionsByAddr(CA_ADDRESS, 0, txParams.LastRound+100)

	for _, tx := range txs.Transactions {
		if tx.From == address {
			//fmt.Println("Already registered with CA...")
			return true
		}
	}
	return false
}

func receiveTransactionsFromAddr(address string, fromAddr string, client algod.Client) []models.Transaction {
	txParams, err := client.SuggestedParams()
	if err != nil {
		fmt.Printf("error getting suggested tx params: %s\n", err)
	}

	/**
	Retrieve first round here, if doesn't exist use 0
	*/
	var txsFromAddr []models.Transaction
	txs, _ := client.TransactionsByAddr(address, 0, txParams.LastRound+100)
	for _, tx := range txs.Transactions {
		if tx.From == fromAddr {
			txsFromAddr = append(txsFromAddr, tx)
		}
	}
	return txsFromAddr
}

func receiveTransactions(address string, client algod.Client) []models.Transaction {
	txParams, err := client.SuggestedParams()
	if err != nil {
		fmt.Printf("error getting suggested tx params: %s\n", err)
	}

	/**
	Retrieve first round here, if doesn't exist use 0
	*/
	var txsFromAddr []models.Transaction
	txs, _ := client.TransactionsByAddr(address, 0, txParams.LastRound+100)
	for _, tx := range txs.Transactions {
		txsFromAddr = append(txsFromAddr, tx)
	}
	return txsFromAddr
}

func receiveTransactionsFromAddrWithLastRound(address string, fromAddr string, client algod.Client, lastRound uint64) []models.Transaction {
	txParams, err := client.SuggestedParams()
	if err != nil {
		fmt.Printf("error getting suggested tx params: %s\n", err)
	}

	/**
	Retrieve first round here, if doesn't exist use 0
	*/
	//fmt.Println(lastRound)
	//fmt.Println(txParams.LastRound)
	var txsFromAddr []models.Transaction
	txs, _ := client.TransactionsByAddr(address, lastRound, txParams.LastRound+100)
	for _, tx := range txs.Transactions {
		if tx.From == fromAddr {
			//fmt.Println(tx.LastRound)
			txsFromAddr = append(txsFromAddr, tx)
		}
	}
	return txsFromAddr
}

func sendTransaction(toAddr string, fromAddr string, mn string, note []byte, client algod.Client) {
	txParams, err := client.SuggestedParams()
	if err != nil {
		fmt.Printf("Error getting suggested tx params: %s\n", err.Error())
		return
	}
	// Make transaction
	genID := txParams.GenesisID
	tx, err := transaction.MakePaymentTxn(fromAddr, toAddr, 0, 0, txParams.LastRound, txParams.LastRound+100, note, "", genID, txParams.GenesisHash)

	if err != nil {
		fmt.Printf("Error creating transaction: %s\n", err.Error())
		return
	}

	fromAddrPvtKey, err := mnemonic.ToPrivateKey(mn)
	if err != nil {
		fmt.Printf("Error getting private key: %s\n", err.Error())
		return
	}
	//fmt.Println("Private key recovered from mnemonic")

	// Sign the Transaction
	_, bytes, err := crypto.SignTransaction(fromAddrPvtKey, tx)
	if err != nil {
		fmt.Printf("Failed to sign transaction: %s\n", err.Error())
		return
	}

	// Broadcast the transaction to the network
	txHeaders := append([]*algod.Header{}, &algod.Header{"Content-Type", "application/x-binary"})
	_, err = client.SendRawTransaction(bytes, txHeaders...)
	if err != nil {
		fmt.Printf("failed to send transaction: %s\n", err.Error())
		return
	}
	//fmt.Printf("Transaction successful with ID: %s\n", sendResponse.TxID)
}

func firstStart(client algod.Client) {
	//Check if directory exists
	didExist := checkIfConfigDirExists()
	if didExist {
		return
	}

	//Open the sqlite database
	algochatDB, _ := sql.Open("sqlite3", os.Getenv("HOME")+"/.config/algochat/algochat.db")
	defer algochatDB.Close() //defer it from closing

	//Create the accounts table if it doesn't exist
	createAccountsTable(algochatDB)
	createAddressBookTable(algochatDB)
	createMessagesTable(algochatDB)

	addAccount(algochatDB, client)
}

func checkIfConfigDirExists() bool {
	_, err := os.Stat(os.Getenv("HOME") + "/.config/algochat")

	//If it doesn't execute this block
	if os.IsNotExist(err) {
		fmt.Print("Seeing first time execution...\nCreating Algochat Config Directory ~/.config/algochat\n")

		errDir := os.MkdirAll(os.Getenv("HOME")+"/.config/algochat", 0755)
		file, err := os.Create(os.Getenv("HOME") + "/.config/algochat/algochat.db") // Create SQLite file

		checkError("Something went wrong creating the algochat SQLite database..", err)
		checkError("Something went wrong creating the algochat config dir..", errDir)

		file.Close()
		return false
	} else {
		return true //assuming there is an account generated, should add a check that the account exists
	}
}

func createAccountsTable(db *sql.DB) {
	//Generate the accounts table query
	createAddressTableQuery := `CREATE TABLE IF NOT EXISTS accounts (
		mnemonic TEXT,
		address TEXT,
		public_key BLOB,
		private_key BLOB
	);`

	//fmt.Println("Creating accounts table..")
	stmt, err := db.Prepare(createAddressTableQuery)
	checkError("Something went wrong preparing the accounts table query..", err)

	stmt.Exec() //Actually execute the query
}

func createAddressBookTable(db *sql.DB) {
	//Generate the address table query
	createAddressTableQuery := `CREATE TABLE IF NOT EXISTS address_book (
		address TEXT,
		public_key BLOB,
		nickname TEXT
	);`

	//fmt.Println("Creating address book table..")
	stmt, err := db.Prepare(createAddressTableQuery)
	checkError("Something went wrong preparing the address book table query..", err)

	stmt.Exec() //Actually execute the query
}

func createMessagesTable(db *sql.DB) {
	//Generate the messages table query
	createMessageTableQuery := `CREATE TABLE IF NOT EXISTS messages (
    	direction TEXT,
		address TEXT,
		message TEXT,
		lastround INTEGER
	);`
	//messages(direction, address, message, lastround)
	//fmt.Println("Creating messages table..")
	stmt, err := db.Prepare(createMessageTableQuery)
	checkError("Something went wrong preparing the messages table query..", err)

	stmt.Exec() //Actually execute the query
}

func generateAccount() (crypto.Account, string) {
	//Generate account
	account := crypto.GenerateAccount()
	newMn, err := mnemonic.FromPrivateKey(account.PrivateKey)
	checkError("Failed to recover mnemonic from private key..", err)

	//fmt.Printf("Address: %s\n", account.Address)

	// Disregard the nil at the end (that's not part of the mnemonic)
	//fmt.Printf("Mnemonic: %s\n", newMn)

	return account, newMn
}

func addAccount(db *sql.DB, client algod.Client) {
	account, newMn := generateAccount()

	//Generate PKI pair
	privateKey, err := rsa.GenerateKey(rand.Reader, 512)
	checkError("Something went wrong creating the RSA Private Key..", err)

	publicKey := &privateKey.PublicKey

	//fmt.Println("Adding newly generated account..")
	insertAccountSQL := `INSERT INTO accounts(mnemonic, address, public_key, private_key) VALUES (?, ?, ?, ?)`

	stmt, err := db.Prepare(insertAccountSQL)

	_, err = stmt.Exec(newMn, account.Address.String(), x509.MarshalPKCS1PublicKey(publicKey), x509.MarshalPKCS1PrivateKey(privateKey))
	checkError("Something went wrong executing the add account statement..", err)
}

func addAccountToBook(db *sql.DB, account AccountFromBook) {
	//fmt.Println("Adding newly inserted account to address book..")
	insertAccountSQL := `INSERT INTO address_book(address, public_key, nickname) VALUES (?, ?, ?)`

	stmt, err := db.Prepare(insertAccountSQL)

	_, err = stmt.Exec(account.Address, account.PublicKey, account.Nickname)
	checkError("Something went wrong executing the add account to address book statement..", err)
}

func addNewMessages(db *sql.DB, client algod.Client) {
	_, address, _, private_key := getAccount(db)
	accountsMessaged := getAccountsFromBook(db)
	privateKey, _ := x509.ParsePKCS1PrivateKey(private_key)
	for _, account := range accountsMessaged {
		//fmt.Println(account.Nickname)
		getMessagesFromAddress(address, account.Address, privateKey, client, db)
	}
}

type ChatMessage struct {
	Direction  string
	Address    string
	Message    string
	FirstRound uint64
}

type ChatMessageTransaction struct {
	EncryptedSecretKey []byte
	EncryptedMessage   []byte
}

func sendMessageToAddress(toAddr string, fromAddr string, mn string, message string, client algod.Client, db *sql.DB) {
	txParams, _ := client.SuggestedParams()
	key := make([]byte, 16)
	_, _ = rand.Read(key)

	account := getAccountFromBook(toAddr, db)
	publicKey, _ := x509.ParsePKCS1PublicKey(account.PublicKey)

	//Encrypt key with RSA public key of recipient
	encryptedKey, _ := rsa.EncryptOAEP(sha1.New(), rand.Reader, publicKey, key, []byte(""))

	cipherText := encryptMessage(message, key)
	//Need to serialize and send message
	messageToSend := ChatMessageTransaction{encryptedKey, cipherText}

	sendTransaction(toAddr, fromAddr, mn, serializeMessage(messageToSend), client)

	//Add message to table for storing chats
	addMessageToTable(db, ChatMessage{"To", toAddr, message, txParams.LastRound})
}

func serializeMessage(message ChatMessageTransaction) []byte {
	return append(message.EncryptedSecretKey, message.EncryptedMessage...)
}

func deserializeMessage(messageBytes []byte) ChatMessageTransaction {
	return ChatMessageTransaction{EncryptedSecretKey: messageBytes[:64], EncryptedMessage: messageBytes[64:]}
}

func getFirstRoundFromAddress(db *sql.DB, fromAddr string) uint64 {
	if len(getMessagesForAddressFromTable(db, fromAddr)) == 0 { //If no messages exist
		return 0
	}
	return getMessagesForAddressSortedFromTable(db, fromAddr)[0].FirstRound //Otherwise grab first message
}

func getMessagesFromAddress(address string, fromAddr string, privateKey *rsa.PrivateKey, client algod.Client, db *sql.DB) {
	transactionsFromAddr := receiveTransactionsFromAddrWithLastRound(address, fromAddr, client, getFirstRoundFromAddress(db, fromAddr))

	var notesFromTransactions []ChatMessage
	for _, tx := range transactionsFromAddr {
		messageDeserialized := deserializeMessage(tx.Note) //Deserialize retrieved message
		//Decrypt AES key using RSA private key
		decryptedKey, _ := rsa.DecryptOAEP(sha1.New(), rand.Reader, privateKey, messageDeserialized.EncryptedSecretKey, []byte(""))

		//Put it in a ChatMessage object
		var chatMessage ChatMessage
		chatMessage.Direction = "From"
		chatMessage.Address = fromAddr
		chatMessage.Message = decryptMessage(messageDeserialized.EncryptedMessage, decryptedKey) //Decrypt AES encryption
		chatMessage.FirstRound = tx.FirstRound

		//Append it to the array
		notesFromTransactions = append(notesFromTransactions, chatMessage)
	}
	messagesCheck := getMessagesForAddressFromTable(db, fromAddr)
	messageClone := false
	for _, message := range notesFromTransactions {
		for _, messageCheck := range messagesCheck {
			if messageCheck.Message == message.Message && messageCheck.FirstRound == message.FirstRound {
				messageClone = true
			}
		}
		if !messageClone {
			addMessageToTable(db, message)
		}
		messageClone = false
	}
}

func encryptMessage(stringToEncrypt string, key []byte) []byte {
	plaintext := []byte(stringToEncrypt)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err.Error())
	}

	aesGCM, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}

	nonce := make([]byte, aesGCM.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		panic(err.Error())
	}
	ciphertext := aesGCM.Seal(nonce, nonce, plaintext, nil)
	return ciphertext
}

func decryptMessage(enc []byte, key []byte) string {
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err.Error())
	}

	aesGCM, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}

	nonceSize := aesGCM.NonceSize()

	nonce, ciphertext := enc[:nonceSize], enc[nonceSize:]

	plaintext, err := aesGCM.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		panic(err.Error())
	}

	return fmt.Sprintf("%s", plaintext)
}

func addMessageToTable(db *sql.DB, message ChatMessage) {
	//fmt.Println("Adding newly sent / retrieved message to address book..")
	insertMessageSQL := `INSERT INTO messages(direction, address, message, lastround) VALUES (?, ?, ?, ?)`

	stmt, err := db.Prepare(insertMessageSQL)

	_, err = stmt.Exec(message.Direction, message.Address, message.Message, message.FirstRound)
	checkError("Something went wrong executing the addMessageToTable statement..", err)
}

func getMessagesForAddressFromTable(db *sql.DB, addressToSearch string) (chatMessagesReturn []ChatMessage) {
	row, err := db.Query("SELECT * FROM messages WHERE address = '" + addressToSearch + "'")

	checkError("Something went wrong executing the getMessagesForAddress query..", err)

	defer row.Close()

	var direction string
	var address string
	var message string
	var lastRound uint64

	var chatMessages []ChatMessage

	for row.Next() {
		row.Scan(&direction, &address, &message, &lastRound)
		chatMessages = append(chatMessages, ChatMessage{direction, address, message, lastRound})
	}
	return chatMessages
}

func getMessagesForAddressSortedFromTable(db *sql.DB, addressToSearch string) (chatMessagesReturn []ChatMessage) {
	row, err := db.Query("SELECT * FROM messages WHERE address = '" + addressToSearch + "' ORDER BY lastround DESC")

	checkError("Something went wrong executing the getMessagesForAddress query..", err)

	defer row.Close()

	var direction string
	var address string
	var message string
	var lastRound uint64

	var chatMessages []ChatMessage

	for row.Next() {
		row.Scan(&direction, &address, &message, &lastRound)
		chatMessages = append(chatMessages, ChatMessage{direction, address, message, lastRound})
	}
	return chatMessages
}

func getMessagesForAddressSortedFromTableASC(db *sql.DB, addressToSearch string) (chatMessagesReturn []ChatMessage) {
	row, err := db.Query("SELECT * FROM messages WHERE address = '" + addressToSearch + "' ORDER BY lastround ASC")

	checkError("Something went wrong executing the getMessagesForAddress query..", err)

	defer row.Close()

	var direction string
	var address string
	var message string
	var lastRound uint64

	var chatMessages []ChatMessage

	for row.Next() {
		row.Scan(&direction, &address, &message, &lastRound)
		chatMessages = append(chatMessages, ChatMessage{direction, address, message, lastRound})
	}
	return chatMessages
}

func getAccount(db *sql.DB) (mnemonicReturn string, addressReturn string, publicKey []byte, privateKey []byte) {
	row, err := db.Query("SELECT * FROM accounts")
	checkError("Something went wrong executing the getAccount query..", err)

	defer row.Close()

	var mnemonic string
	var address string
	var public_key []byte
	var private_key []byte

	for row.Next() {

		row.Scan(&mnemonic, &address, &public_key, &private_key)

		//fmt.Println("Account: ", mnemonic, "\n", address, "\n", publicKey, "\n", privateKey)
	}
	return mnemonic, address, public_key, private_key
}

type AccountFromBook struct {
	Address   string
	PublicKey []byte
	Nickname  string
}

func getAccountsFromBook(db *sql.DB) []AccountFromBook {
	row, err := db.Query("SELECT * FROM address_book")

	checkError("Something went wrong executing the getAccountsFromBook query..", err)

	defer row.Close()

	var address string
	var public_key []byte
	var nickname string

	var accounts []AccountFromBook

	for row.Next() {
		row.Scan(&address, &public_key, &nickname)
		accounts = append(accounts, AccountFromBook{address, public_key, nickname})
		//fmt.Println("Account: ", mnemonic, "\n", address, "\n", publicKey, "\n", privateKey)
	}
	return accounts
}

func getAccountFromBook(fromAddr string, db *sql.DB) AccountFromBook {
	row, err := db.Query("SELECT * FROM address_book WHERE address = '" + fromAddr + "'")

	checkError("Something went wrong executing the getAccountFromBook query..", err)

	defer row.Close()

	var address string
	var public_key []byte
	var nickname string

	var account AccountFromBook

	for row.Next() {
		row.Scan(&address, &public_key, &nickname)
		account = AccountFromBook{address, public_key, nickname}
		//fmt.Println("Account: ", mnemonic, "\n", address, "\n", publicKey, "\n", privateKey)
	}
	return account
}

func getAddressFromNicknameFromBook(nicknameToSearch string, db *sql.DB) AccountFromBook {
	//fmt.Println(nicknameToSearch)
	row, err := db.Query("SELECT * FROM address_book WHERE nickname = '" + nicknameToSearch + "'")

	checkError("Something went wrong executing the getAddressFromNicknameFromBook query..", err)

	defer row.Close()

	var address string
	var public_key []byte
	var nickname string

	var account AccountFromBook

	for row.Next() {
		row.Scan(&address, &public_key, &nickname)
		account = AccountFromBook{address, public_key, nickname}
		//fmt.Println("Account: ", mnemonic, "\n", address, "\n", publicKey, "\n", privateKey)
	}
	return account
}

func checkError(errorMessage string, err error) {
	if err != nil {
		fmt.Printf(errorMessage)
		log.Fatal(err.Error())
	}
}
