# **Algochat**


## Introduction
	

A secure decentralized messaging application based upon the blockchain Algorand. Written using Golang, Algorand SDK, TermUI, go-sqlite3, and a clipboard library.

## Installation


* Make sure Golang is installed and your $GOPATH is set properly.
* To make sure clipboard functionality works install xclip. Useful for copying address / mnemonic / pasting in addresses or messages.
* go get -u github.com/gizak/termui/...
* go get github.com/atotto/clipboard
* go get github.com/mattn/go-sqlite3
* go get -u github.com/algorand/go-algorand-sdk/...